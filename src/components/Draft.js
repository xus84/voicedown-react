import React from "react";

export const Draft = () => {
  return (
    <div>
      <textarea placeholder="Write your note" rows="4" cols="50" />
    </div>
  );
};
