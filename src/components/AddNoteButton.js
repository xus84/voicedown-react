import React, { useState } from "react";
import ReactTooltip from "react-tooltip";
import { Draft } from "./Draft";

export const AddNoteButton = () => {
  const [Notes, setNotes] = useState([]);

  const addNotes = () => {
    setNotes(Notes.concat(<Draft />));
    console.log("hello");
  };

  return (
    <>
      <button onClick={addNotes} data-tip data-for="registerTip">
        +
      </button>
      {Notes}
      <ReactTooltip id="registerTip" place="right" effect="solid">
        Add a new note
      </ReactTooltip>
    </>
  );
};
