import React from "react";
import { AddNoteButton } from "./components/AddNoteButton";
import { Draft } from "./components/Draft";
import "./App.css";

function App() {
  return (
    <div className="App">
      <h3>Voicedown</h3>
      <AddNoteButton />
      <Draft />
    </div>
  );
}

export default App;

